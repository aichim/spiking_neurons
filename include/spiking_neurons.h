#pragma once

#include <opencv2/opencv.hpp>

class SpikingNeurons
{
public:
  SpikingNeurons ();

  bool loadSequence (const std::string &folder_sequence);

  void quantifyDifferences (cv::Mat &img_a, cv::Mat &img_b, cv::Mat &img_res);

  void processPair (const int &index);
  void produceSpikeSequence (cv::Mat &img_diffs);

  std::vector<cv::Mat*> sequence_;
  cv::Mat *image_left_, *image_right_, *image_diffs_;
  std::vector<cv::Mat*> spikes_;

  int window_length_, timestep_;
  int neuron_image_rows_, neuron_image_cols_, input_image_rows_, input_image_cols_;
};