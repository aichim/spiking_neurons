#pragma once

#include <QWidget>
#include <QTimer>
#include "ui_spiking_neurons_widget.h"

#include "spiking_neurons.h"


class SpikingNeuronsWidget : public QWidget
{
Q_OBJECT
public:
  SpikingNeuronsWidget (const std::string &folder_sequence);

  void processFrame ();
  void playSpikeSequence ();

  Ui::SpikingNeuronsWidget ui_;


  int index_current_;
  SpikingNeurons sn_;
  QTimer timer_animation_;
  int animation_index_;

private slots:
  void animateDraw ();
  void on_button_previous_clicked();
  void on_button_next_clicked();
  void on_button_play_spikes_clicked();

};