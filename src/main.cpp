#include <QApplication>

#if 1

#include "spiking_neurons_widget.h"

int
main (int argc,
      char **argv)
{
  QApplication qapp (argc, argv);

  SpikingNeuronsWidget snw (argv[1]);
  snw.setVisible (true);

  return (qapp.exec ());
}


#endif


#if 0

#include <cvimagewidget.h>

#include <QDialog>
#include <QApplication>
#include <QMainWindow>

int main(int argc, char** argv) {
  QApplication app(argc, argv);
  QMainWindow window;

  // Create the image widget
  CVImageWidget* imageWidget = new CVImageWidget();
  window.setCentralWidget(imageWidget);

  // Load an image
  cv::Mat image = cv::imread(argv[1], true);
  imageWidget->showImage(image);

  window.show();

  return app.exec();
}

#endif