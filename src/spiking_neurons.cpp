#include "spiking_neurons.h"
#include <QDir>

SpikingNeurons::SpikingNeurons ()
  : image_left_(nullptr), image_right_(nullptr), image_diffs_(nullptr)
  , window_length_ (500), timestep_ (20)
{

}

bool
SpikingNeurons::loadSequence (const std::string &folder_sequence)
{
  // Get the list of all files of type PNG from given folder
  QDir dir (folder_sequence.c_str ());
  QStringList files_list = dir.entryList ({"*.png"});

  // Read every 5th (so that the motions are larger and the results are more visual)
  for (int i = 0; i < files_list.size (); i += 5)
  {
    cv::Mat img_in = cv::imread (folder_sequence + "/" + files_list.at (i).toStdString ());
    cv::Mat* img = new cv::Mat ();

    // Downscale the images so they fit the GUI
    resize (img_in, *img, cv::Size (img_in.cols / 3, img_in.rows / 3));

    sequence_.push_back (img);
  }

  // Our actual processing resolution is 1/4 of the image resolution (speed and visualization reasons)
  neuron_image_rows_ = sequence_[0]->rows / 4;
  neuron_image_cols_ = sequence_[0]->cols / 4;
  input_image_rows_ = sequence_[0]->rows;
  input_image_cols_ = sequence_[0]->cols;

  std::cout << "Loaded " << files_list.size () << " images.\n";

  return (true);
}


void
SpikingNeurons::processPair (const int &index)
{
  image_left_ = sequence_[index];
  image_right_ = sequence_[index + 1];

  if (image_diffs_ == nullptr)
    image_diffs_ = new cv::Mat ();


  // Downscale the images for processing
  cv::Mat img_left_small, img_right_small;
  resize (*image_left_, img_left_small, cv::Size (neuron_image_cols_, neuron_image_rows_));
  resize (*image_right_, img_right_small, cv::Size (neuron_image_cols_, neuron_image_rows_));

  // Convert them to grayscale
  cv::Mat img_left_small_bw, img_right_small_bw;
  cvtColor (img_left_small, img_left_small_bw, CV_RGB2GRAY);
  cvtColor (img_right_small, img_right_small_bw, CV_RGB2GRAY);

  // Take the absolute differences
  cv::Mat img_small_diffs;
  absdiff (img_left_small_bw, img_right_small_bw, img_small_diffs);

  // Upscale the image for display purposes
  resize (img_small_diffs, *image_diffs_, cv::Size (image_left_->cols, image_left_->rows));

  // Generate the temporal spike sequence
  produceSpikeSequence (img_small_diffs);
}

void
SpikingNeurons::produceSpikeSequence (cv::Mat &img_diffs)
{
  // Initialize the spike sequence images - all black for now
  int time_quantification = window_length_ / timestep_;
  spikes_.resize (time_quantification);
  for (size_t i = 0; i < spikes_.size (); ++i)
  {
    if (spikes_[i] == nullptr)
      spikes_[i] = new cv::Mat (neuron_image_rows_, neuron_image_cols_, CV_8UC1);

    spikes_[i]->setTo (0);
  }

  const int threshold = 55;
  // Go through each pixel
  for (int i = 0; i < img_diffs.rows; ++i)
    for (int j = 0; j < img_diffs.cols; ++j)
    {
      int pixel = img_diffs.at<uchar> (i, j);

      // Filter out small values (hardcoded threshold = 55)
      if (pixel >= threshold)
      {
        /**
         Our range is now from 55 - 255 => 200 discrete steps
         255 corresponds to the highest frequency spiking we can do: 111111111 ...
         <=55 corresponds to all neurons off: 00000 ...
         middle 155 corresponds to half frequency: 1010101010 ...
         **/

        double num_spikes = static_cast<double> (pixel - threshold) / (255 - threshold) * time_quantification;
        int spike_freq = static_cast<double> (time_quantification) / num_spikes;

        // Now set the corresponding neural response pixels, based on the frequency
        for (size_t im_i = 0; im_i < spikes_.size (); im_i += spike_freq)
          spikes_[im_i]->at<uchar> (i, j) = 255;
      }
    }
}