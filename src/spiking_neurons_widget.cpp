#include "spiking_neurons_widget.h"

#include <QMainWindow>

SpikingNeuronsWidget::SpikingNeuronsWidget (const std::string &folder_sequence)
{
  sn_.loadSequence (folder_sequence);


  ui_.setupUi (this);

  index_current_ = 0;
  processFrame ();

  setVisible (true);


  std::cerr << "image size: " << sn_.sequence_[0]->rows << " " << sn_.sequence_[0]->cols << "\n";

  timer_animation_.setSingleShot (true);
  connect (&timer_animation_, SIGNAL (timeout ()), this, SLOT (animateDraw ()));
}


void
SpikingNeuronsWidget::processFrame ()
{
  // Do the actual image processing
  sn_.processPair (index_current_);

  // Display the static images
  ui_.viewer_input_left->showImage (*sn_.image_left_);
  ui_.viewer_input_right->showImage (*sn_.image_right_);
  ui_.viewer_intensity->showImage (*sn_.image_diffs_);

  // Play the spike sequence for this image pair
  playSpikeSequence ();
}

void
SpikingNeuronsWidget::playSpikeSequence ()
{
  animation_index_ = 0;
  timer_animation_.start (sn_.timestep_ * 5);
}

void
SpikingNeuronsWidget::animateDraw ()
{
  if (animation_index_ < sn_.window_length_ / sn_.timestep_)
  {
    // Upscale the image for visualization purposes
    cv::Mat img;
    cv::resize (*sn_.spikes_[animation_index_], img, cv::Size (sn_.input_image_cols_, sn_.input_image_rows_));
    ui_.viewer_spikes->showImage (img);
    timer_animation_.start (sn_.timestep_ * 5);
  }
  animation_index_ ++;
}

void
SpikingNeuronsWidget::on_button_previous_clicked ()
{
  std::cerr << "PREVIOUS\n";

  index_current_ = std::max (0, index_current_ - 1);
  processFrame ();
}

void
SpikingNeuronsWidget::on_button_next_clicked ()
{
  std::cerr << "NEXT\n";

  index_current_ = std::min (static_cast<int> (sn_.sequence_.size () - 2), index_current_ + 1);
  processFrame ();
}

void
SpikingNeuronsWidget::on_button_play_spikes_clicked()
{
  std::cerr << "PLAY SPIKES\n";
  playSpikeSequence ();
}